
#pragma once

template<typename TStatType, typename TDataType>
class TYPEMod : public PackObj, public PackableJson
{
public:

	virtual void Pack(class BinaryWriter *pWriter) override; 
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;

	int _unk;
	int _operationType;
	TStatType _stat;
	TDataType _value;
};

template<typename TStatType, typename TDataType>
class TYPERequirement : public PackObj, public PackableJson	
{
public:

	virtual void Pack(class BinaryWriter *pWriter) override;
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;


	TStatType _stat;
	TDataType _value;
	int _operationType;
	std::string _message;
};

class CraftRequirements : public PackObj, public PackableJson
{
public:
	DECLARE_PACKABLE()
	DECLARE_PACKABLE_JSON();

	PackableListWithJson<TYPERequirement<STypeInt, int>> _intRequirement;
	PackableListWithJson<TYPERequirement<STypeDID, uint32_t>> _didRequirement;
	PackableListWithJson<TYPERequirement<STypeIID, uint32_t>> _iidRequirement;
	PackableListWithJson<TYPERequirement<STypeFloat, double>> _floatRequirement;
	PackableListWithJson<TYPERequirement<STypeString, std::string>> _stringRequirement;
	PackableListWithJson<TYPERequirement<STypeBool, BOOL>> _boolRequirement;
};

class CraftMods : public PackObj, public PackableJson
{
public:

	virtual void Pack(class BinaryWriter *pWriter) override;
	virtual bool UnPack(class BinaryReader *pReader) override;
	virtual void PackJson(json& writer) override;
	virtual bool UnPackJson(const json& reader) override;

	PackableListWithJson<TYPEMod<STypeInt, int>> _intMod;
	PackableListWithJson<TYPEMod<STypeDID, uint32_t>> _didMod;
	PackableListWithJson<TYPEMod<STypeIID, uint32_t>> _iidMod;
	PackableListWithJson<TYPEMod<STypeFloat, double>> _floatMod;
	PackableListWithJson<TYPEMod<STypeString, std::string>> _stringMod;
	PackableListWithJson<TYPEMod<STypeBool, BOOL>> _boolMod;

	int _ModifyHealth=0;
	int _ModifyStamina=0;
	int _ModifyMana=0;
	int _RequiresHealth=0;
	int _RequiresStamina=0;
	int _RequiresMana=0;

	bool _unknown7 = false;
	uint32_t _modificationScriptId = 0 ;

	int _unknown9 = 0;
	uint32_t _unknown10 = 0;
};

class CraftOperationData : public PackableJson
{
public:
	DECLARE_PACKABLE_JSON();

	uint32_t _unk = 0;
	STypeSkill _skill = STypeSkill::UNDEF_SKILL;
	int _difficulty = 0;
	uint32_t _SkillCheckFormulaType = 0;
	uint32_t _successWcid = 0;
	uint32_t _successAmount = 0;
	std::string _successMessage;
	uint32_t _failWcid = 0;
	uint32_t _failAmount = 0;
	std::string _failMessage;

	double _successConsumeTargetChance;
	int _successConsumeTargetAmount;
	std::string _successConsumeTargetMessage;

	double _successConsumeToolChance;
	int _successConsumeToolAmount;
	std::string _successConsumeToolMessage;

	double _failureConsumeTargetChance;
	int _failureConsumeTargetAmount;
	std::string _failureConsumeTargetMessage;

	double _failureConsumeToolChance;
	int _failureConsumeToolAmount;
	std::string _failureConsumeToolMessage;

	CraftRequirements _requirements[3];
	CraftMods _mods[8];

	uint32_t _dataID = 0;
};

class CCraftOperation : public PackObj, public CraftOperationData
{
public:
	DECLARE_PACKABLE()

};

class JsonCraftOperation : public CraftOperationData
{
public:
	JsonCraftOperation() = default;
	JsonCraftOperation(const CraftOperationData& other)
		: CraftOperationData(other), _recipeID()
	{
	}
	JsonCraftOperation(const CraftOperationData& other, uint32_t id)
		: CraftOperationData(other), _recipeID(id)
	{
	}

	DECLARE_PACKABLE_JSON();

	uint32_t _recipeID = 0;
};

class CCraftTable : public PackObj, public PackableJson
{
public:
	CCraftTable();
	virtual ~CCraftTable() override;

	DECLARE_PACKABLE()
	DECLARE_PACKABLE_JSON();
	

	PackableHashTable<uint32_t, CCraftOperation> _operations;
	PackableHashTable<uint64_t, uint32_t, uint64_t> _precursorMap;
};

class CraftPrecursor : public PackableJson
{
public:
	CraftPrecursor() = default;
	CraftPrecursor(uint32_t recipe, uint32_t tool, uint32_t target) :
		RecipeID(recipe), Tool(tool), Target(target)
	{ }

	DECLARE_PACKABLE_JSON();

	uint32_t Tool = 0;
	uint32_t Target = 0;
	uint32_t RecipeID = 0;
	uint64_t ToolTargetCombo = 0;

};

