#include <StdAfx.h>
#include "easylogging++.h"
#include "Client.h"
#include "WeenieObject.h"
#include "Monster.h"
#include "Player.h"
#include "World.h"
#include "Config.h"
#include "Server.h"
#include "ClientCommands.h"

CLIENT_COMMAND(setvoidconfig, "[multipler]", "Used to limit void damage 0.1 = 10%, 1.0 = 100%", ADMIN_ACCESS, SERVER_CATEGORY)
{
	if (argc < 1)
	{
		pPlayer->SendText("Multiplier missing", LTT_DEFAULT);
		return true;
	}
	double newValue = atof(argv[0]);
	g_pConfig->SetVoidDamageReduction(newValue);

	pPlayer->SendText(csprintf("Void Damage set to %f of max", newValue), LTT_DEFAULT);

	return false;
}


